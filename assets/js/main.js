/*
 new image cordinates:
 width: 526 px;
 height:    600 px;

 range of latitude - 56 to 104
 range of longitude   -  0 to 40

 for 1 unit latitude  move  left to right   526/48 = 10.96 px
 for 1 unit longitude move top to  bottom 600/40 = 15 px
 */
$(window).on('load', function () {


    var map = new function () {

        this.latitudeMultiple = 64.90;// width of map / range of latitude
        this.longitudeMultiple = 51.68;// height of map / range of longitude
        this.latitudeOffset = 4;// latitude starting value on map ( x - axis)
        this.longitudeOffset = 68;// longitude starting value on map (y - axis)

        // X - axis
        this.setLatitude = function (latitude) {
                latitude -= this.latitudeOffset;
                return this.latitudeMultiple * latitude;
        };

        // Y - axis
        this.setLongitude = function (longitude) {
                longitude -= this.longitudeOffset;
                return this.longitudeMultiple * longitude;
        };

        /**
         *
         * @param{number latitude
         * @param number longitude
         * @param number zipCode
         * creates new marker element and set marker for  provided lat,lng values
         */
        this.setMarker = function (latitude, longitude, zipCode) {


            var latitudePosition = this.setLatitude(latitude);

            var longitudePosition = this.setLongitude(longitude);

            var markerHtml = "<img width=30 id='" + zipCode 
                    + "' src='assets/images/location-mark.png'>";

            // create new element
            var markerElement = $(markerHtml).insertBefore("#map");
            if (longitudePosition == 0
                || typeof latitudePosition == 'undefined'
                || typeof longitudePosition == 'undefined') {
                return false;
            }

            latitudePosition = latitudePosition + 'px';
            longitudePosition = longitudePosition + 'px';

            var styleAttr = '';
            var markerStyle = {
                "position": "absolute",
                "top": latitudePosition,
                "left": longitudePosition

            };

            for (var key in markerStyle) {
                if (markerStyle.hasOwnProperty(key))
                        styleAttr += (key + ": " + markerStyle[key] + "; ");
            }

            var elementAttributes = {};
            elementAttributes.style = styleAttr;
            elementAttributes.id = zipCode;
            elementAttributes.title = zipCode;
            markerElement.attr(elementAttributes);

        };
    };

    var locationData = {
        '0': {
                lat: 8,
                lng: 72,
                zipCode: 110001
        },
        '1': {
                lat: 10,
                lng: 72.1,
                zipCode: 110002
        },
        '2': {
                lat: 11,
                lng: 72.2,
                zipCode: 110003
        },

        '3': {
                lat: 12,
                lng: 70,
                zipCode: 600001
        }
    };

    for (var key in locationData) {
map.setMarker(locationData[key].lat, locationData[key].lng, 
locationData[key].zipCode);
    }
});